import axios from 'axios';

const Api = axios.create({
	baseURL: 'http://localhost:3001',
});

export const Criar = async project => {
	if(project.editar){
		delete project.editar
		const response = await Api.put (`/projetos/editar/${project.id}`, project);
		return response
	}else{
		const response = await Api.post('/projetos/criar', project);
		return response;
	}
};

export const Listar = async () => {
	const response = await Api.get('/projetos');
	//console.log(response.data)
	return response.data;
};

export const Delete = async (id) => {
	const response = await Api.get(`/projetos/deletar/${id}`)
	console.log(response)
	return response.data;
};