import React from 'react'
import {Link} from 'react-router-dom'

const Login = () => (
  <div className="container" style={{maxWidth: 400}}>
    <form className="form-signin" action="sistema/index.php"  method="post">
        <img className="mb-4" src="sistema/themes/cenex/img/logo-ntw.svg" alt="" width="auto" height="50" />
        <h1 className="h3 mb-3 font-weight-normal">Intranet::CENEX</h1>
        <label htmlFor="inputEmail" className="sr-only">Endereço de email</label>
        <input type="email" id="inputEmail" className="form-control" placeholder="Usuário" autoFocus />
        <label htmlFor="inputPassword" className="sr-only">Senha</label>
        <input type="password" id="inputPassword" className="form-control" placeholder="Senha" />
        <button className="btn btn-lg btn-primary btn-block" type="submit">Login</button>
        <p className="mt-3">
            <Link to="#">Novo usuário</Link> <b>|</b> <Link to="#">Esqueceu sua senha?</Link>
        </p>
        <p className="mt-5 mb-3 text-muted">FAO UFMG &copy; 2018-2019</p>
    </form>
  </div>
)

export default Login;