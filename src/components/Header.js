import React from 'react';
import { Link } from 'react-router-dom'

const Header = () => {
  return (
    <div>
      <nav className="navbar navbar-expand-md navbar-dark fixed-top ntw-bg">
        <Link className="logo-cenex" to="/"><img src="img/logo-fao-ufmg.svg" width="150px" height="40px" alt="Logo CENEX FAO" /></Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul className="navbar-nav mr-auto">
                <li className="nav-item active"><Link className="nav-link" to="/">Início</Link></li>
                <li className="nav-item dropdown">
                    <Link className="nav-link dropdown-toggle" to='/' id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ações de Extensão</Link>
                    <div className="dropdown-menu" aria-labelledby="dropdown01">
                        <Link className="dropdown-item" to="/projetos">Listar ações</Link>
                        <Link className="dropdown-item" to="/criar">Criar ação</Link>
                        <Link className="dropdown-item" to="/">Editar ações</Link>
                        <Link className="dropdown-item" to="/">Remover ações</Link>
                    </div>
                </li>
                <li className="nav-item"><Link className="nav-link" to="/">Certificados</Link></li>
                <li className="nav-item"><Link className="nav-link" to="/">Ajuda <i className="far fa-question-circle"></i></Link></li>
            </ul>
            <p className="text-white">Intranet :: <strong>FAO UFMG</strong></p>
        </div>
    </nav>
    <div className="navbar fixed-top sombra container-fluid">
        <ul className="nav">
            <li className="nav-item">Você está em:</li>
            <li><i className="fas fa-home"></i> Início</li>
        </ul>
        <ul className="nav  justify-content-end">
            <li className="nav-item">Usuário: </li>
            <li className="nav-item"><strong>Nome</strong></li>
            <li><Link to="/"><i className="fas fa-sign-out-alt"></i></Link></li>
        </ul>
    </div>
    </div>
  );
};

export default Header;
